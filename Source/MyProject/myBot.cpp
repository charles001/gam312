// Fill out your copyright notice in the Description page of Project Settings.

#include "myBot.h"


// Sets default values
AmyBot::AmyBot()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AmyBot::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmyBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AmyBot::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

