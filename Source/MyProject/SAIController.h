// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SAIController.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ASAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AController(const FObjectInitializer& ObjectInitializer);
	Add the custom follow path component and the following to the constructor :
	AController::AController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UJoyPathFollowComp>(TEXT("PathFollowingComponent"))) {
	
		PublicDependencyModuleNames.AddRange(new string[]{
	"Core", "CoreUObject", "Engine", "InputCore" , "Landscape", "UMG", "PhysX", "APEX", "AIModule");
	Override the following functions in the pathfollowing component :
	virtual void FollowPathSegment(float DeltaTime) override;
	virtual void SetMoveSegment(int32 SegmentStartIndex) override;
	virtual void UpdatePathSegment() override;
};
